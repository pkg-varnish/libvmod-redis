#
# This is a VCL file for Varnish. It tests the "redis" vmod.
#

vcl 4.0;

import redis;

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "127.0.0.1";
    .port = "80";
}

sub vcl_init {
    redis.init("main", "127.0.0.1:6379", 500, 0, 0, 0, 0, false, 1);
}

sub vcl_recv {
    redis.command("SET");
    redis.push("foo");
    redis.push("Hello world!");
    redis.execute();
    if (!redis.reply_is_status()) {
            return(synth(500, "reply_is_status() failed"));
    }
    redis.free();

    redis.command("GET");
    redis.push("foo");
    redis.execute();
    if (!redis.reply_is_string()) {
        return(synth(501, "reply_is_string() failed"));
    }
    redis.free();

    redis.command("EVAL");
    redis.push({"
        redis.call('SET', KEYS[1], ARGV[1]);
        redis.call('EXPIRE', KEYS[1], ARGV[2]);
    "});
    redis.push("1");
    redis.push("bar");
    redis.push(req.xid);
    redis.push("1");
    redis.execute();
    if (!redis.reply_is_nil()) {
        return(synth(502, "reply_is_nil()"));
    }
    redis.free();

    return(synth(200, "redis vmod ok"));
}
